When this script is run, it will check a mailbox for any unread "go-live" messages from Twitch.  If found, it will send a message to a hipchat room to let the room know.  All messages will then be marked as read.  

creds.py.example should be renamed to creds.py and the variables set correctly.  

The email USER and PASS are needed.  As well as the V2API Key for the hipchat user who will post the message and the ROOMID that the message will be posted to. 

For my own use, I run this via a cronjob that checks every few minutes.  