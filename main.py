#!/usr/bin/python -tt

import getmail
import send_alert

liveuser = getmail.check_mail()
if liveuser is not None:
    send_alert.send_alert_to_hipchat(liveuser)

