import imaplib
import re
import creds

def check_mail():
    myname = None
    obj = imaplib.IMAP4_SSL('imap.gmail.com','993')
    obj.login(creds.EMAIL_USER, creds.EMAIL_PASS)
    obj.select()

    result, data = obj.search(None, "UNSEEN")
    ids = data[0] # data is a list.
    id_list = ids.split() # ids is a space separated string
    #print id_list

    for email_id in id_list:  
        result, data = obj.fetch(email_id, "(RFC822)") # fetch the email body (RFC822) for the given ID
        Subjects = re.search("Subject: (.+) just went live on Twitch", data[0][1])
        if Subjects is not None:
            myname = Subjects.group(1)

    return myname
    
    obj.expunge() # Mark all as read
    obj.close()
    obj.logout()

if __name__ == '__main__':
    check_mail()
