#!/usr/bin/python -tt
import json
from urllib2 import Request, urlopen
import creds

V2TOKEN = creds.V2TOKEN
ROOMID = creds.ROOMID

def send_alert_to_hipchat(name):
    # API V2, send message to room:
    url = 'https://api.hipchat.com/v2/room/%d/notification' % ROOMID
    message = str(name) + " just went live on Twitch!  https://www.twitch.tv/" + str(name)
    headers = {
                "content-type": "application/json",
                "authorization": "Bearer %s" % V2TOKEN}
    datastr = json.dumps({
            'message': message,
            'color': 'green',
            'message_format': 'text',
            'notify': False})
    request = Request(url, headers=headers, data=datastr)
    uo = urlopen(request)
    rawresponse = ''.join(uo)
    uo.close()
    assert uo.code == 204
